skybot next generation
----------------------

(O_O)

## What is this?
Due to SkyBot being extremely hard to read, we have remade SkyBot.

## What language?
C, we all know and love C.

## How to install?
On GNU, just run `make`. You will find an executable in `bin/`
On BSD, you have to explicitly specify the makefile. In order to do this, run `make -f BSDmakefile`

## What modules? 
openssl

