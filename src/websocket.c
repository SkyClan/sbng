#include "websocket.h"

String *
WebSocket_key_generate (void)
{
	// a random string (uninitialized is random enough)
	int bytes[WS_GENERATE_KEY_SIZE]; 

	// encode and return
	return base64_encode(String_ncreate((char *) bytes, sizeof(bytes)));
} 
