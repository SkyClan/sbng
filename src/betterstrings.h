#ifndef __betterstrings_h__
#define __betterstrings_h__

#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "dbg.h"

typedef struct String {
	char *str;
	size_t len;
	size_t bs;
} String;

String *String_ncreate(char *, size_t);
void String_concat(String *, String *);
int String_expand_by(String *, size_t);

#define String_expand(M) String_expand_by(M, 300)
#define String_create(M) String_ncreate(M, strlen(M))
#define String_destroy(M) { free(M->str); free(M); }

#endif
