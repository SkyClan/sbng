#ifndef __websocket_h__
#define __websocket_h__

#include "betterstrings.h"
#include "base64.h"

#define WS_MAGIC_STRING "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define WS_GENERATE_KEY_SIZE 16

String *WebSocket_key_generate(void);

#endif
