#include "betterstrings.h"

String *
String_ncreate (char *str, size_t len)
{
	check(str, "String can't be NULL");
	check(len, "Length can't be NULL");

	String *retStr = malloc(sizeof(String));
	check(retStr, "malloc failed.");
	// handle

	retStr->bs = len;
	retStr->len = len;
	retStr->str = malloc(retStr->bs + 1);
	retStr->str[retStr->bs] = '\0';

	strncpy(retStr->str, str, len);

	return retStr;
error:
	return NULL;
}


int
String_expand_by (String *str, size_t by)
{
	check(str, "String can't be NULL");

	str->bs += by ? by : 300;
	str->str = realloc(str->str, str->bs + 1);
	str->str[str->bs] = '\0';

	check(str->str, "realloc failed.");
	
	return 0;
error:
	return 1;
} 

void
String_concat (String *thisStr, String *thatStr)
{
	if (thisStr->bs < thisStr->len + thatStr->len) 
		check(String_expand_by(thisStr, thatStr->len) == 0, "Failed to expand string.");

	// run a strncat
	strncat(thisStr->str, thatStr->str, thatStr->len);

	String_destroy(thatStr);
error:
	return;
}

