#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

#include "dbg.h"
#include "betterstrings.h"
#include "websocket.h"

#define GATEWAY_HOSTNAME "gateway.discord.gg:https"
#define TOKEN_FILE "tools/token.txt"
#define CLEANUP(A, B) { SSL_CTX_free(A); BIO_free_all(B); }

int sock_init(BIO *);

int 
main (void)
{
	SSL_load_error_strings();
	SSL_library_init();

	const SSL_METHOD *method = SSLv23_client_method();
	check(method, "SSLv3_client_method failed.");

	SSL_CTX *ctx = SSL_CTX_new(method);
	check(ctx, "SSL_CTX_new failed.");

	BIO *bio = BIO_new_ssl_connect(ctx);
	check(bio, "BIO_new_ssl_connect failed.");

	SSL *ssl = NULL;

	BIO_get_ssl(bio, &ssl);
	SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
	BIO_set_conn_hostname(bio, GATEWAY_HOSTNAME);

	check(BIO_do_connect(bio) > 0, "BIO_do_connect failed."); // waiting here
	check(SSL_CTX_load_verify_locations(ctx, "/etc/ssl/certs/ca-certificates.crt", "/etc/ssl/certs/"), "SSL_CTX_load_verify_locations failed.");

	long verify_flag = SSL_get_verify_result(ssl);
	if (verify_flag != X509_V_OK ) 
		log_warn("Certificate verification error %d but continuing...", (int) verify_flag);



	check(sock_init(bio), "Initializing socket failed.");

	CLEANUP(ctx, bio);
	return 0;

error: 
	CLEANUP(ctx, bio);
	return 1;
}

/* return 1 on success, 0 on failure */
int
sock_init(BIO *bio)
{
	FILE *token_file;

	BIO_puts(bio,
		"GET /?v=10&encoding=json HTTP/1.1\r\n"
		"Host: discord.gg\r\n"
		"Connection: upgrade\r\n"
		"Upgrade: websocket\r\n"
		"Sec-WebSocket-Version: 13\r\n"
		"Sec-WebSocket-Key: "
	);

	String *wskey = WebSocket_key_generate();
	BIO_puts(bio, wskey->str);
	String_destroy(wskey);

	// the token stuff actually shouldnt belong here.
	BIO_puts(bio, "\r\n"
			"Token: ");

	check(token_file = fopen(TOKEN_FILE, "r"), "Unable to make the token file");

	// get the length of the file
	fseek(token_file, 0, SEEK_END);

	// allocate data
	String *token = String_ncreate("", ftell(token_file));

	// update the data
	fseek(token_file, 0, SEEK_SET);
	fgets(token->str, token->len, token_file);

	BIO_puts(bio, token->str);
	BIO_puts(bio, "\r\n\r\n");

	String_destroy(token);

	char response[1024];
	
	while (1) {
		memset(response, '\0', sizeof(response));
		if (BIO_read(bio, response, sizeof(response)) <= 0) break;

		puts(response);
	}

	return 1;
error:
	return 0;
}
