#include "base64.h"

String *                    
base64_encode (String *data)
{                                                                          
	size_t length = ENCODED_LENGTH(data->len);
                                                                            
	String *encoded_data = String_ncreate("", length);
	check(encoded_data, "Failed to create encoded_data String.");

	for (size_t i = 0, j = 0; i < data->len;) {
		uint32_t octet_a = i < data->len ? (unsigned char)data->str[i++] : 0;
		uint32_t octet_b = i < data->len ? (unsigned char)data->str[i++] : 0;
		uint32_t octet_c = i < data->len ? (unsigned char)data->str[i++] : 0;

		uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

		encoded_data->str[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
		encoded_data->str[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
		encoded_data->str[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
		encoded_data->str[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
	}

	for (int i = 0; i < mod_table[data->len % 3]; i++)
		encoded_data->str[length - 1 - i] = '=';

	// free the original String
	String_destroy(data);

	return encoded_data;
error:
	return 0;
} 
